# ========================================================================
# main.mk for purelearning
# ========================================================================


PUREC = pure
PUREC_OPTS =  -I../lib -w -v3

all: $(EXE)

$(EXE): $(SOURCES)
	$(PUREC) $(PUREC_OPTS) -c $(MAIN).pure -o $(EXE)

clean:
	rm -fv $(EXE)

testbasic: all
	./$(EXE)
	./$(EXE) -v
	./$(EXE) -h

test: testbasic
	./$(EXE) $(TEST_ARGS)

.PHONY: all clean testbasic test